#![no_std]
#![cfg_attr(test, no_main)]
#![feature(abi_x86_interrupt)]
#![feature(alloc_error_handler)]

// This feature requires no external libraries
// and thus also works in #[no_std] environments.
// It works by collecting all functions annotated with
// a #[test_case] attribute and then invoking a user-specified
// runner function with the list of tests as argument.
//
// There is currently a bug in cargo that leads to “duplicate lang item” errors
// on cargo test in some cases.
// It occurs when you have set panic = "abort" for a profile in your Cargo.toml.
// Try removing it, then cargo test should work.
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]

// The custom test frameworks feature generates a main function
// that calls test_runner, but this function is ignored because
// we use the #[no_main] attribute and provide our own entry point.
// To fix this, we need to change the name of the generated function.
#![reexport_test_harness_main = "test_main"]


extern crate alloc;


pub mod vga_buffer;
pub mod serial;
pub mod interrupts;
pub mod gdt;
pub mod memory;
pub mod allocator;


use core::panic::PanicInfo;

#[cfg(test)]
use bootloader::{BootInfo, entry_point};


/* ========================================================================== */
/*                                    QEMU                                    */
/* ========================================================================== */


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum QemuExitCode {
    Success = 0x10,
    Failed  = 0x11,
}

pub fn exit_qemu(exit_code: QemuExitCode) {
    use x86_64::instructions::port::Port;

    unsafe {
        let mut port = Port::new(0xf4);
        port.write(exit_code as u32);
    }
}


/* ========================================================================== */
/*                                   TESTS                                    */
/* ========================================================================== */


pub trait Testable {
    fn run(&self) -> ();
}

impl <T> Testable for T
    where
        T: Fn(),
{
    fn run(&self) {
        // For functions, the type is their name
        serial_print!("{}...\t", core::any::type_name::<T>());
        self();
        serial_println!("[ok]");
    }
}


pub fn test_runner(tests: &[&dyn Testable]) {
    serial_println!("Running {} tests", tests.len());
    for test in tests {
        test.run();
    }
    exit_qemu(QemuExitCode::Success);
}

pub fn test_panic_handler(info: &PanicInfo) -> ! {
    serial_println!("[failed]\n");
    serial_println!("Error: {}\n", info);
    exit_qemu(QemuExitCode::Failed);
    hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    serial_println!("[failed]\n");
    serial_println!("Error: {}\n", info);
    exit_qemu(QemuExitCode::Failed);
    loop {}
}


/* ========================================================================== */
/*                                   COMMON                                   */
/* ========================================================================== */


pub fn init() {
    gdt::init();
    interrupts::init_idt();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();
}

pub fn hlt_loop() -> ! {
    loop {
        // This allows the CPU to enter a sleep state
        // in which it consumes much less energy
        x86_64::instructions::hlt();
    }
}

#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}


/* ========================================================================== */
/*                                    MAIN                                    */
/* ========================================================================== */


#[cfg(test)]
entry_point!(test_kernel_main);

#[cfg(test)]
fn test_kernel_main(_boot_info: &'static BootInfo) -> ! {
    init();
    test_main();
    hlt_loop();
}