#![no_std]
#![no_main]

// This feature requires no external libraries
// and thus also works in #[no_std] environments.
// It works by collecting all functions annotated with
// a #[test_case] attribute and then invoking a user-specified
// runner function with the list of tests as argument.
//
// There is currently a bug in cargo that leads to “duplicate lang item” errors
// on cargo test in some cases.
// It occurs when you have set panic = "abort" for a profile in your Cargo.toml.
// Try removing it, then cargo test should work.
#![feature(custom_test_frameworks)]
#![test_runner(os::test_runner)]

// The custom test frameworks feature generates a main function
// that calls test_runner, but this function is ignored because
// we use the #[no_main] attribute and provide our own entry point.
// To fix this, we need to change the name of the generated function.
#![reexport_test_harness_main = "test_main"]


use core::panic::PanicInfo;
use os::println;


#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();

    loop {}
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    os::test_panic_handler(info);
}


#[test_case]
fn test_println() {
    println!("test_println output");
}